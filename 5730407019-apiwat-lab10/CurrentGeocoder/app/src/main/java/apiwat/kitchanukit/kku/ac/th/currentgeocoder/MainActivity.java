package apiwat.kitchanukit.kku.ac.th.currentgeocoder;

import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MainActivity extends FragmentActivity implements  GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private double currentLatitude;
    private double currentLongitude;
    RadioGroup group;
    RadioButton rb_address,rb_ll;
    EditText et_latitude,et_longitude,et_address;
    Button fetch;
    String address;

    double latitude,longitude;
    TextView text_address;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        text_address = (TextView) findViewById(R.id.text_address);

        fetch = (Button) findViewById(R.id.bt_fetch);
        group = (RadioGroup) findViewById(R.id.group);


        et_latitude = (EditText) findViewById(R.id.et_latitude);
        et_longitude = (EditText) findViewById(R.id.et_longitude);
        et_address = (EditText) findViewById(R.id.et_address);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.

        text_address = (TextView) findViewById(R.id.text_address);

        fetch = (Button) findViewById(R.id.bt_fetch);
        group = (RadioGroup) findViewById(R.id.group);

        et_latitude = (EditText) findViewById(R.id.et_latitude);
        et_longitude = (EditText) findViewById(R.id.et_longitude);
        et_address = (EditText) findViewById(R.id.et_address);

        rb_address = (RadioButton) findViewById(R.id.rb_useadd);
        rb_ll = (RadioButton) findViewById(R.id.rb_uselatlang);

        fetch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(rb_ll.isChecked()) {

                    latitude = Double.parseDouble(et_latitude.getText().toString());
                    longitude = Double.parseDouble(et_longitude.getText().toString());
                    getAddress(latitude,longitude);

                }else if(rb_address.isChecked()){
                    address = et_address.getText().toString();
                    getLaLng(address);
                }
            }
        });
        rb_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                et_address.setEnabled(true);
                et_longitude.setEnabled(false);
                et_latitude.setEnabled(false);

                et_address.requestFocus();

            }
        });

        rb_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(rb_ll.isChecked()){

                    et_address.setEnabled(false);
                    et_longitude.setEnabled(true);
                    et_latitude.setEnabled(true);
                    et_latitude.requestFocus();

                }
            }
        });

        buildGoogleApiClient();
    }



    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new
                GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
        mGoogleApiClient.connect();
    }
    @Override
    protected void onPause() {
        super.onPause();
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mGoogleApiClient != null &&
                mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
        }
    }


    @Override
    public void onConnected(Bundle bundle) {
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (location == null) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
        else {
            handleNewLocation(location);
        }

    }

    private void handleNewLocation(Location location) {

        currentLatitude = location.getLatitude();
        currentLongitude = location.getLongitude();
        getAddress(currentLatitude,currentLongitude);
        et_latitude.setText(String.valueOf(currentLatitude));
        et_longitude.setText(String.valueOf(currentLongitude));
    }

    @Override
    public void onConnectionSuspended(int i) {

    }


    @Override
    public void onLocationChanged(Location location) {
        handleNewLocation(location);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }
    private void getAddress(double latitude, double longitude) {

        try {

            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses.size() > 0 ) {
                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String local = addresses.get(0).getLocality();

                et_address.setText(address + ", " + local);
                text_address.setText(address + ", " + local);
            }


        } catch (IOException e) {
            Log.e("tag", e.getMessage());
        }
    }
    private void getLaLng(String address){
        Geocoder coder = new Geocoder(this);
        try {
            ArrayList<Address> adresses = (ArrayList<Address>) coder.getFromLocationName(address,50);
            for(Address add : adresses){

                double longitude = add.getLongitude();
                double latitude = add.getLatitude();
                text_address.setText("Lat:"+ latitude + " " + "Lng:"+longitude);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}