package apiwat.kitchanukit.kku.ac.th.simplegeocoder;

import android.app.Activity;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MainActivity extends Activity {

    RadioGroup group;
    RadioButton rb_address,rb_ll;
    EditText et_latitude,et_lngtitude,et_address;
    Button fetch;
    String address;

    double latitude,longitude;
    TextView text_address;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        text_address = (TextView) findViewById(R.id.text_address);

        fetch = (Button) findViewById(R.id.bt_fetch);
        group = (RadioGroup) findViewById(R.id.group);

        et_latitude = (EditText) findViewById(R.id.et_latitude);
        et_lngtitude = (EditText) findViewById(R.id.et_longtitude);
        et_address = (EditText) findViewById(R.id.et_address);

        rb_address = (RadioButton) findViewById(R.id.rb_useadd);
        rb_ll = (RadioButton) findViewById(R.id.rb_uselatlang);

        fetch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(rb_ll.isChecked()) {

                    latitude = Double.parseDouble(et_latitude.getText().toString());
                    longitude = Double.parseDouble(et_lngtitude.getText().toString());
                    getAddress(latitude,longitude);

                }else if(rb_address.isChecked()){
                    address = et_address.getText().toString();
                    getLaLng(address);
                }
            }
        });
        rb_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                    et_address.setEnabled(true);
                    et_lngtitude.setEnabled(false);
                    et_latitude.setEnabled(false);

                    et_address.requestFocus();

            }
        });

        rb_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(rb_ll.isChecked()){

                    et_address.setEnabled(false);
                    et_lngtitude.setEnabled(true);
                    et_latitude.setEnabled(true);
                    et_latitude.requestFocus();

                }
            }
        });
    }
    private void getAddress(double latitude, double longitude) {

        try {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses.size() > 0) {
                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String local = addresses.get(0).getLocality();

                text_address.setText(address + ", "+local);

            }
        } catch (IOException e) {
            Log.e("tag", e.getMessage());
        }
    }
    private void getLaLng(String address){
        Geocoder coder = new Geocoder(this);
        try {
            ArrayList<Address> adresses = (ArrayList<Address>) coder.getFromLocationName(address,50);
            for(Address add : adresses){

                    double longitude = add.getLongitude();
                    double latitude = add.getLatitude();
                    text_address.setText("Lat:"+ latitude + " " + "Lng:"+longitude);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

