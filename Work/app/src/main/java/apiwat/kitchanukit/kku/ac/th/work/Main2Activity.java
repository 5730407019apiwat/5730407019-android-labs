package apiwat.kitchanukit.kku.ac.th.work;

import android.app.Activity;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.TextView;

public class Main2Activity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main2);

        Bundle bundle = getIntent().getExtras();
        int score = bundle.getInt("score");

        TextView text1 = (TextView) findViewById(R.id.text1);
        TextView text2 = (TextView) findViewById(R.id.text2);
        TextView color = (TextView) findViewById(R.id.color);


        if(score < 2){
            color.setBackgroundColor(Color.parseColor("#a5cf00")); //green score less than 2
            text1.setText("ไม่มีอาการผิดปกติ ไปพบแพทย์ตามนัดครั้งถัดไป");
        }else if(score >= 2 && score <10){
            color.setBackgroundColor(Color.parseColor("#f0ff3d")); //yellow score more than 2 and less than 10
            text1.setText("เฝ้าระวังอาการต่อไป หรือ ถ้ามีข้อสงสัยโทรปรึกษาได้ที่เบอร์ 02-7634017");
        }else if(score >= 10){
            color.setBackgroundColor(Color.parseColor("#ff291f")); //red score more than 10
            text1.setText("ไปโรงพยาบาล พร้อมกับเอกสาร");
            text2.setText("1. สมุดบันทึกสุขภาพแม่และเด็ก\n2.บัตรประจำตัวประชาชน\n3.สำเนาทะเบียนบ้านสำหรับทำสูติบัตรลูก(แจ้งเกิด)");
        }
    }
}
