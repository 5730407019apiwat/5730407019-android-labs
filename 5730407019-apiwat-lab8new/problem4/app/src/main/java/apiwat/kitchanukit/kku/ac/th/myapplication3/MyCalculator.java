package apiwat.kitchanukit.kku.ac.th.myapplication3;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;


public class MyCalculator extends Activity {

    RadioGroup group;
    RadioButton add, minus, multiply, divide;
    EditText ed1, ed2;
    TextView text;
    Button cal;
    double num1, num2, sum;
    Toast toast;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_calculator);


        ed1 = (EditText) findViewById(R.id.ed1);
        ed2 = (EditText) findViewById(R.id.ed2);
        text = (TextView) findViewById(R.id.text);


        //radio
        group = (RadioGroup) findViewById(R.id.group);
        add = (RadioButton) findViewById(R.id.add);// +
        minus = (RadioButton) findViewById(R.id.minus);//-
        multiply = (RadioButton) findViewById(R.id.multiply);//*
        divide = (RadioButton) findViewById(R.id.divide);// /


        cal = (Button) findViewById(R.id.cal);//Button Calculator


        //check Radio
        group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                acceptNumber();

                calculate(i);
            }

        });

        //Check Button Calculator
        cal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                acceptNumber();
                calculate(group.getCheckedRadioButtonId());

            }
        });

        //Check Switch


        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;

        showToast("Width = " + width +"and Height = " + height);
    }
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        // Save UI state changes to the savedInstanceState.
        // This bundle will be passed to onCreate if the process is
        // killed and restarted.

        savedInstanceState.putString("sum", text.getText().toString());
        savedInstanceState.putString("edit1", ed1.getText().toString());
        savedInstanceState.putString("edit2", ed2.getText().toString());
        // etc.
        super.onSaveInstanceState(savedInstanceState);
    }
    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        text.setText(savedInstanceState.getString("sum"));
        ed1.setText(savedInstanceState.getString("edit1"));
        ed2.setText(savedInstanceState.getString("edit2"));
    }


    //Calculate
    public void calculate(int id) {

        long start = System.currentTimeMillis();

        switch (id) {

            case R.id.add:
                sum = num1 + num2;
                break;
            case R.id.minus:
                sum = num1 - num2;
                break;
            case R.id.multiply:
                sum = num1 * num2;
                break;
            case R.id.divide:

                if (num2 == 0) {
                    sum = 0;
                    showToast("Please divide by a non-zero number");
                    break;
                }
                sum = num1 / num2;
                break;

        }

        text.setText("= " + String.valueOf(sum));

        long end = System.currentTimeMillis();

        android.util.Log.d("TimeClass", "TimeValue" + (start - end));

        Intent I = new Intent(MyCalculator.this,SecondActivity.class);

        I.putExtra("result",String.valueOf(sum));
        startActivity(I);
    }


    //Show Toast
    public void showToast(String msg) {
        toast = Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG);
        toast.show();

    }

    //accept input
    private void acceptNumber() {

        try {
            num1 = Double.parseDouble(ed1.getText().toString());
            num2 = Double.parseDouble((ed2.getText().toString()));
        } catch (NumberFormatException e) {
            showToast("Please enter only a number");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_settings) {
            showToast("Choose action settings");
            return true;
        }else
            return super.onOptionsItemSelected(item);

    }
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


}
