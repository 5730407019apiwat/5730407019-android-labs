package apiwat.kitchanukit.kku.ac.th.problem3;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {
    EditText edit_name;
    TextView text_name;
    Button btn_ok;
    private static final int REQUEST_CODE = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        text_name = (TextView) findViewById(R.id.text_name);
        btn_ok = (Button) findViewById(R.id.btn_ok);


        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edit_name = (EditText) findViewById(R.id.edit_name);
                Intent i = new Intent(MainActivity.this, Result.class);
                i.putExtra("text_main",edit_name.getText().toString());
                startActivityForResult(i, REQUEST_CODE);
            }


        });



    }

    @Override
    protected void onActivityResult(
            int requestCode, int resultCode, Intent data) {

        super.onActivityResult ( requestCode, resultCode, data );
        if (resultCode == RESULT_OK && requestCode == REQUEST_CODE) {

            String text_return = data.getStringExtra("return");
            text_name.setHint(text_return);

        }


    }
}
