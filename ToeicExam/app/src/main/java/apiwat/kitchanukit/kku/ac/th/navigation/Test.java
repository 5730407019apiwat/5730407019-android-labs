package apiwat.kitchanukit.kku.ac.th.navigation;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.firebase.client.Firebase;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

/**
 * Created by pc1 on 23/4/2560.
 */
@TargetApi(Build.VERSION_CODES.GINGERBREAD)
@SuppressLint("newApi")
public class Test extends Activity implements View.OnClickListener {

    TextView choice1, choice2, choice3, choice4, problem_text, part, time,text_question;

    Button submit,btnPause;
    String reading_num;
    int num = 0, count = 1, ans,count_read;
    int score = 0;
    long millis;
    private StringBuilder builder;
    String listText, x, answers;
    private FirebaseUser user;
    FirebaseAuth firebaseAuth;
    JSONArray part_array;
    private Firebase mRef;
    int i = 1;
    String hms;
    boolean isPause = false,isCancel = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test);


        firebaseAuth = FirebaseAuth.getInstance();
        user = firebaseAuth.getCurrentUser();
        mRef = new Firebase("https://toeic-a77a0.firebaseio.com/" + user.getUid());

        Bundle bundle = getIntent().getExtras();
        listText = bundle.getString("part");
        choice1 = (TextView) findViewById(R.id.choice1);
        choice2 = (TextView) findViewById(R.id.choice2);
        choice3 = (TextView) findViewById(R.id.choice3);
        choice4 = (TextView) findViewById(R.id.choice4);

        problem_text = (TextView) findViewById(R.id.problem);
        text_question= (TextView) findViewById(R.id.text_question);
        part = (TextView) findViewById(R.id.part);

        time = (TextView) findViewById(R.id.time);

        btnPause = (Button)findViewById(R.id.pause);
        submit = (Button) findViewById(R.id.submit);
        submit.setOnClickListener(this);
        btnPause.setOnClickListener(this);

        Resources res = getResources();
        InputStream is = res.openRawResource(R.raw.exam_toeic);
        Scanner scanner = new Scanner(is);

        builder = new StringBuilder();

        while (scanner.hasNextLine()) {
            builder.append(scanner.nextLine());
        }

        parseJson(builder.toString(), 0, listText);
        //time

        if (count == 1) {
            time.setText("00:01:00");
            final Test.CounterClass timer = new Test.CounterClass(60000, 1000);
            timer.start();
            count = 2;
        }

    }

    private void parseJson(String s, int i, String list) {


        num = i;

        if (list.equals("Incomplete Sentence")) {
            x = "Incomplete-sentence";
            text_question.setVisibility(View.INVISIBLE);
             part.setText(list);
        } else if (list.equals("Vocabulary Test")) {
            x = "VocabularyExamination";
            text_question.setVisibility(View.INVISIBLE);
            part.setText(list);
        }else if(list.equals("Reading Comprehension")){
            x= "Reading-Comprehension";
            part.setText(list);
            text_question.setVisibility(View.VISIBLE);
        }
        try {

            if(x.equals("Reading-Comprehension")){
                if(num == 0){
                    reading_num = "reading1";
                    count_read = 0;
                }else if(num == 3){
                    count_read = 3;
                    reading_num = "reading2";
                }else if(num+1 == 9){
                    count_read = 8;
                    reading_num = "reading3";
                }else if(num+1 == 12){
                    count_read = 11;
                    reading_num = "reading4";
                }else if(num+1 == 17){
                    count_read = 16;
                    reading_num = "reading5";
                }
                JSONObject root = new JSONObject(s);
                JSONArray toeic = (JSONArray) root.get("ToeicExam");
                part_array = (JSONArray) toeic.getJSONObject(0).get(x);
                String reading = part_array.getJSONObject(count_read).getString(reading_num);
                String question = part_array.getJSONObject(num).getString("problem" + String.valueOf(num+1));
                String choice1s = part_array.getJSONObject(num).getString("choice1");
                String choice2s = part_array.getJSONObject(num).getString("choice2");
                String choice3s = part_array.getJSONObject(num).getString("choice3");
                String choice4s = part_array.getJSONObject(num).getString("choice4");
                answers = part_array.getJSONObject(num).getString("answernum");

                text_question.setText(i + 1 + ". " + question);

                choice1.setText("A. "+choice1s);
                choice2.setText("B. "+choice2s);
                choice3.setText("C. "+choice3s);
                choice4.setText("D. "+choice4s);
                problem_text.setText(reading);
            }else {
                JSONObject root = new JSONObject(s);
                JSONArray toeic = (JSONArray) root.get("ToeicExam");
                part_array = (JSONArray) toeic.getJSONObject(0).get(x);
                String problems = part_array.getJSONObject(num).getString("problem" + String.valueOf(num + 1));
                String choice1s = part_array.getJSONObject(num).getString("choice1");
                String choice2s = part_array.getJSONObject(num).getString("choice2");
                String choice3s = part_array.getJSONObject(num).getString("choice3");
                String choice4s = part_array.getJSONObject(num).getString("choice4");
                answers = part_array.getJSONObject(num).getString("answernum");

                problem_text.setText(i + 1 + ". " + problems);
                choice1.setText("A. "+choice1s);
                choice2.setText("B. "+choice2s);
                choice3.setText("C. "+choice3s);
                choice4.setText("D. "+choice4s);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onClick(View view) {

         if (view == choice1) {          ///choice1
            ans = 1;
            if (ans == Integer.valueOf(answers)) {
                score++;
            }
            num++;
            parseJson(builder.toString(), num, listText);
        } else if (view == choice2) {
            ans = 2;
            if (ans == Integer.valueOf(answers)) {
                score++;
            }
             num++;
            parseJson(builder.toString(), num, listText);

        } else if (view == choice3) {
            ans = 3;
            if (ans == Integer.valueOf(answers)) {
                score++;
            }

            num++;
            parseJson(builder.toString(), num, listText);
        } else if (view == choice4) {
            ans = 4;
            if (ans == Integer.valueOf(answers)) {
                score++;
            }
            num++;
            parseJson(builder.toString(), num, listText);

        } else if (view == submit) {
            isPause = true;
            parseScore();
        }else if(view == btnPause){
             if(isPause == false) {
                 isPause = true;
             }else {
                 isPause = false;
                 final Test.CounterClass timer = new Test.CounterClass(60000, 1000);
                 timer.start();
             }
         }
    }


    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    @SuppressLint("newApi")
    public class CounterClass extends CountDownTimer {

        public CounterClass(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @TargetApi(Build.VERSION_CODES.GINGERBREAD)
        @SuppressLint("newApi")
        @Override
        public void onTick(long l) {

            if(isPause){
                cancel();
            }else {
                 millis = l;
                 hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
                        TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                        TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
                System.out.print(hms);
                time.setText(hms);
            }

        }

        @Override
        public void onFinish() {
            time.setText("Completed");
            parseScore();

        }
    }


   public void parseScore(){
       if(listText.equals("Incomplete Sentence")) {
           Intent intent = new Intent(Test.this, MainActivity.class);
           intent.putExtra("part","Incomplete");
           intent.putExtra("score", score);
           startActivity(intent);
       }else if(listText.equals("Text Completion")) {
           Intent intent = new Intent(Test.this, MainActivity.class);
           intent.putExtra("part","TextCompletion");
           intent.putExtra("score", score);
           startActivity(intent);
       }else if(listText.equals("Reading Comprehension")){
           Intent intent = new Intent(Test.this, MainActivity.class);
           intent.putExtra("part","ReadingComprehension");
           intent.putExtra("score", score);
           startActivity(intent);
       }else if(listText.equals("Vocabulary Test")) {
           Intent intent = new Intent(Test.this, MainActivity.class);
           intent.putExtra("part", "Vocabulary");
           intent.putExtra("score", score);
           startActivity(intent);
       }

   }


}
