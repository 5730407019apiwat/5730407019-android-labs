package apiwat.kitchanukit.kku.ac.th.navigation;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.Scanner;

/**
 * Created by pc1 on 11/5/2560.
 */

public class ShowAnswer extends Activity implements View.OnClickListener{



    EditText question,textAnswer;
    ImageButton back,next;
    JSONArray part_array;
    int num = 0;
    private StringBuilder builder;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_answer);

        question = (EditText) findViewById(R.id.question);
        textAnswer = (EditText) findViewById(R.id.answer);
        back = (ImageButton) findViewById(R.id.back);
        next = (ImageButton) findViewById(R.id.next);

        Resources res = getResources();
        InputStream is = res.openRawResource(R.raw.exam_toeic);
        Scanner scanner = new Scanner(is);

        builder = new StringBuilder();

        while (scanner.hasNextLine()) {
            builder.append(scanner.nextLine());
        }

        parseJson(builder.toString(),0);
    }

    private void parseJson(String s, int num) {



        try {

            JSONObject root = new JSONObject(s);
            JSONArray toeic = (JSONArray) root.get("ToeicExam");
            part_array = (JSONArray) toeic.getJSONObject(0).get("Incomplete-sentence");
            String problems = part_array.getJSONObject(num).getString("problem" + String.valueOf(num + 1));
            String answer = part_array.getJSONObject(num).getString("answer");


            question.setText(num + 1 + ". " + problems);
            textAnswer.setText("Answer : " + answer);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {

        /////back
        if (view == back) {
            num--;
            if(num < 0){
                num = 0;
            }
            parseJson(builder.toString(), num);
            /////////////next
        } else if (view == next) {

            num++;
            if(num > part_array.length()-1){
                num = part_array.length();
            }

            parseJson(builder.toString(), num);
        }
    }


}
