package apiwat.kitchanukit.kku.ac.th.navigation;

/**
 * Created by pc1 on 25/4/2560.
 */

public class UserInformation {

    public String name;
    public String address;

    public UserInformation(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public UserInformation(){

    }
}
