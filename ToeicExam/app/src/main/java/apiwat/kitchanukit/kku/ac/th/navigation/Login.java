package apiwat.kitchanukit.kku.ac.th.navigation;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

/**
 * Created by pc1 on 23/4/2560.
 */

public class Login extends Activity implements View.OnClickListener{

    private Button buttonLogin;
    private EditText editTextEMail;
    private EditText editTextPassword;
    private TextView textViewSignup;
    private Toast toast;
    private Button buttonRegister;
    private CheckBox remember;
    private String value_email,value_pass;
    private ProgressDialog progressDialog;
    private SharedPreferences loginPreferences;
    private SharedPreferences.Editor loginPrefsEditor;
    private Boolean saveLogin;
    private FirebaseAuth firebaseAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        firebaseAuth = FirebaseAuth.getInstance();

        remember = (CheckBox) findViewById(R.id.remember);
        progressDialog = new ProgressDialog(this);
        buttonLogin = (Button) findViewById(R.id.buttonLogin);
        buttonRegister = (Button) findViewById(R.id.buttonRegister);

        editTextEMail = (EditText) findViewById(R.id.editTextEmail);
        editTextPassword = (EditText) findViewById(R.id.editTextPassword);

        //textViewSignup = (TextView) findViewById(R.id.textViewSigin);

        buttonLogin.setOnClickListener(this);

        loginPreferences = getSharedPreferences("loginPrefs", MODE_PRIVATE);
        loginPrefsEditor = loginPreferences.edit();

        buttonLogin.setOnClickListener(this);
        buttonRegister.setOnClickListener(this);

        saveLogin = loginPreferences.getBoolean("saveLogin", false);
        if (saveLogin == true) {
            editTextEMail.setText(loginPreferences.getString("email", ""));
            editTextPassword.setText(loginPreferences.getString("password", ""));
            remember.setChecked(true);
        }


    }
    @Override
    public void onBackPressed() {
        System.exit(0);
        this.finish();
        super.onBackPressed();
    }
    public void showToast(String msg) {
        toast = Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG);
        toast.show();

    }

    private void userLogin(){
        String email = editTextEMail.getText().toString().trim();
        String password = editTextPassword.getText().toString().trim();

        if(TextUtils.isEmpty((email))){
            showToast("Please enter email");
            return;
        }
        if(TextUtils.isEmpty(password)){
            showToast("Please enter password");
            return;
        }

        progressDialog.setMessage("Registering User...");
        progressDialog.show();

        firebaseAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        progressDialog.dismiss();
                        if(task.isSuccessful()){
                            if(remember.isChecked()){
                                value_email = editTextEMail.getText().toString();
                                value_pass = editTextPassword.getText().toString();
                            }else{
                                value_pass = null;
                                value_email = null;
                            }
                            finish();
                            startActivity(new Intent(getApplicationContext(), MainActivity.class));

                        }else {
                            showToast("Could not register please try agin");
                        }
                    }
                });
    }
    @Override
    public void onClick(View view){
        if(view == buttonLogin){
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(editTextEMail.getWindowToken(), 0);

            value_email = editTextEMail.getText().toString();
            value_pass = editTextPassword.getText().toString();

            if (remember.isChecked()) {
                loginPrefsEditor.putBoolean("saveLogin", true);
                loginPrefsEditor.putString("email", value_email);
                loginPrefsEditor.putString("password", value_pass);
                loginPrefsEditor.commit();
            } else {
                loginPrefsEditor.clear();
                loginPrefsEditor.commit();
            }

            userLogin();

        }
            if(view == buttonRegister){
                startActivity(new Intent(Login.this,Register.class));
            }


    }

}
