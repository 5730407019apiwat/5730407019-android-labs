package apiwat.kitchanukit.kku.ac.th.navigation;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

/**
 * Created by pc1 on 11/5/2560.
 */

public class ListAnswer extends Activity{

    ListView list_answer;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_answer);

        list_answer = (ListView) findViewById(R.id.listView);

        list_answer.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(ListAnswer.this, ShowAnswer.class);
                startActivity(intent);
            }
        });
    }
}
