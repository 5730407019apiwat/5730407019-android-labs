package apiwat.kitchanukit.kku.ac.th.navigation;

import android.app.Application;

import com.firebase.client.Firebase;

/**
 * Created by pc1 on 25/4/2560.
 */

public class FireApp extends Application {

    @Override
    public void onCreate(){
        super.onCreate();

        Firebase.setAndroidContext(this);
    }
}
