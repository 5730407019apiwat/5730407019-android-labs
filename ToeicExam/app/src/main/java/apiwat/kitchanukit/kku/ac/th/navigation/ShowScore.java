package apiwat.kitchanukit.kku.ac.th.navigation;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by pc1 on 30/4/2560.
 */

public class ShowScore extends Activity {

    private Firebase mRef;
    private FirebaseUser user;
    ListView mListView;
    TextView score_t;
    Firebase childRef;
    Button result;
    int i=1;
    private ArrayList<String> mScore = new ArrayList<>();
    DatabaseReference databaseReference;
    FirebaseAuth firebaseAuth;
    int score,num_score;
    String time;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);

        firebaseAuth = FirebaseAuth.getInstance();
        user = firebaseAuth.getCurrentUser();
        mRef = new Firebase("https://toeic-a77a0.firebaseio.com/" + user.getUid());

        final Bundle bundle = getIntent().getExtras();
        score = bundle.getInt("score");
        num_score = bundle.getInt("countScore");
        time = bundle.getString("time");

        result = (Button) findViewById(R.id.result);

        childRef  = mRef.child("score" + String.valueOf(num_score));
        childRef.setValue(String.valueOf(score) + "    " + time + " New Score");

        score_t = (TextView) findViewById(R.id.score);

        mListView = (ListView) findViewById(R.id.list_score);

        databaseReference = FirebaseDatabase.getInstance().getReference();

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout
                .simple_list_item_1,mScore);

        mListView.setAdapter(arrayAdapter);

        mRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                String value = dataSnapshot.getValue(String.class);
                mScore.add("Your Score is   "+value);
                childRef.setValue(String.valueOf(score) + "    " + time);
                arrayAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

        result.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(view == result){


                    Bundle bundle1 = getIntent().getExtras();
                    int scoreIncom = bundle1.getInt("scoreIncom");
                    int scoreReading  = bundle1.getInt("scoreReading");
                    int scoreVocab = bundle1.getInt("scoreVocab");
                    Intent intent = new Intent(ShowScore.this,Result.class);
                    intent.putExtra("scoreIncom",scoreIncom);
                    intent.putExtra("scoreReading",scoreReading);
                    intent.putExtra("scoreVocab",scoreVocab);

                    startActivity(intent);
                }
            }
        });


    }
}
