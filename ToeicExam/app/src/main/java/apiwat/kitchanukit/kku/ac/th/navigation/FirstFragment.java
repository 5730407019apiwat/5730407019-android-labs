package apiwat.kitchanukit.kku.ac.th.navigation;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Created by pc1 on 23/4/2560.
 */

public class FirstFragment extends Fragment {

    View myView;

    private ListView mListView;
    private ArrayList<String> part = new ArrayList<>();
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        myView = inflater.inflate(R.layout.first_layout,container, false);


        return myView;
    }


}
