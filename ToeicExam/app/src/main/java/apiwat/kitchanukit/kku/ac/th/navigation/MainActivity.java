package apiwat.kitchanukit.kku.ac.th.navigation;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;


import com.firebase.client.Firebase;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class MainActivity extends Activity
        implements NavigationView.OnNavigationItemSelectedListener {

    private FirebaseAuth firebaseAuth;
    String timeStamp;
    TextView text_email;
    protected DrawerLayout drawer;
    private String str_part;
    private ListView mListView;
    private Firebase mRef;
    private FirebaseUser user;
    int i =1,sum;
    private ArrayList<String> part = new ArrayList<>();
    private DatabaseReference databaseReference;
    public static final String SAVE = "SaveNumberScore";

    private EditText editTextName, editTexAddress;
    Button submit,result;
    private Button buttonSave;
    TextView score_inCom,score_reading,score_vocab;
    int scoreIncom,scoreText,scoreReading,scoreVocab;
    int loadIncom,loadText,loadReading,loadVocab;

    int c_score;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        firebaseAuth = FirebaseAuth.getInstance();
        user = firebaseAuth.getCurrentUser();
        mRef = new Firebase("https://toeic-a77a0.firebaseio.com/" + user.getUid());

        submit = (Button) findViewById(R.id.btn_submit);

        score_inCom = (TextView) findViewById(R.id.score_inCom);
        //score_text = (TextView) findViewById(R.id.score_text);
        score_reading = (TextView) findViewById(R.id.score_reading);
        score_vocab = (TextView) findViewById(R.id.score_vocab);

        Bundle bundle = getIntent().getExtras();

        if(bundle != null) {
            String part = bundle.getString("part");
            if(part.equals("Incomplete")) {

                scoreIncom = bundle.getInt("score");
                SharedPreferences number_score = getSharedPreferences(SAVE,MODE_PRIVATE);
                SharedPreferences.Editor editor = number_score.edit();
                editor.putInt("SaveIncom", scoreIncom);
                editor.apply();
               // score_inCom.setText(String.valueOf(scoreIncom));
            }else if(part.equals("TextCompletion")) {

                scoreText = bundle.getInt("score");
                SharedPreferences number_score = getSharedPreferences(SAVE,MODE_PRIVATE);
                SharedPreferences.Editor editor = number_score.edit();
                editor.putInt("SaveText", scoreText);
                editor.apply();

                //score_text.setText(String.valueOf(score));
            }else if(part.equals("ReadingComprehension")){
                scoreReading = bundle.getInt("score");
                scoreText = bundle.getInt("score");
                SharedPreferences number_score = getSharedPreferences(SAVE,MODE_PRIVATE);
                SharedPreferences.Editor editor = number_score.edit();
                editor.putInt("SaveRead", scoreReading);
                editor.apply();
            }else if(part.equals("Vocabulary")) {
                scoreVocab = bundle.getInt("score");
                scoreText = bundle.getInt("score");
                SharedPreferences number_score = getSharedPreferences(SAVE, MODE_PRIVATE);
                SharedPreferences.Editor editor = number_score.edit();
                editor.putInt("SaveVocab", scoreVocab);
                editor.apply();
            }

        }else{
            SharedPreferences number_score = getSharedPreferences(SAVE, MODE_PRIVATE);
            SharedPreferences.Editor editor = number_score.edit();
            editor.putInt("SaveIncom", 0);
            editor.putInt("SaveText", 0);
            editor.putInt("SaveRead", 0);
            editor.putInt("SaveVocab", 0);
            editor.apply();
        }
        SharedPreferences loadScore = getSharedPreferences(SAVE,0);
        loadIncom = loadScore.getInt("SaveIncom",0);
        loadText = loadScore.getInt("SaveText",0);
        loadReading = loadScore.getInt("SaveRead",0);
        loadVocab = loadScore.getInt("SaveVocab",0);
        score_inCom.setText(String.valueOf(loadIncom));
        //score_text.setText(String.valueOf(loadText));
        score_vocab.setText(String.valueOf(loadVocab));
        score_reading.setText(String.valueOf(loadReading));

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //(toolbar);


        //FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
       /* fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);
        View headerView = navigationView.getHeaderView(0);

        text_email = (TextView) headerView.findViewById(R.id.text_email);
        firebaseAuth = FirebaseAuth.getInstance();

        if(firebaseAuth.getCurrentUser() == null){
            startActivity(new Intent(this, Login.class));


        }

        //textWelcome = (TextView) findViewById(R.id.textWelcome);
        user = firebaseAuth.getCurrentUser();
        text_email.setText(user.getEmail());

        //startActivity(new Intent(MainActivity.this,Save.class));
        mListView = (ListView) findViewById(R.id.listView);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {


                    Intent intent = new Intent(MainActivity.this, Test.class);
                    intent.putExtra("part", mListView.getItemAtPosition(i).toString());
                    startActivity(intent);

            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(view == submit){
                    //save text to 0
                    SharedPreferences number_score = getSharedPreferences(SAVE,MODE_PRIVATE);
                    SharedPreferences.Editor editor = number_score.edit();
                    editor.putInt("SaveIncom", 0);
                    editor.putInt("SaveText", 0);
                    editor.putInt("SaveRead", 0);
                    editor.putInt("SaveVocab", 0);
                    editor.apply();


                    int sum_incom = Integer.valueOf(score_inCom.getText().toString());
                    int sum_vocab = Integer.valueOf(score_vocab.getText().toString());
                    int sum_reading = Integer.valueOf(score_reading.getText().toString());
                   // int sum_reading = Integer.valueOf(score_reading.getText().toString());
                   // int sum_vocab = Integer.valueOf(score_vocab.getText().toString());

                    score_inCom.setText("0");
                    score_vocab.setText("0");
                    score_reading.setText("0");

                    sum = sum_incom + sum_reading + sum_vocab;
                    createScore();
                    Intent intent = new Intent(MainActivity.this,ShowScore.class);
                    intent.putExtra("score",sum);
                    intent.putExtra("countScore",c_score);
                    intent.putExtra("time",timeStamp);
                    intent.putExtra("scoreIncom",sum_incom);
                    intent.putExtra("scoreReading",sum_reading);
                    intent.putExtra("scoreVocab",sum_vocab);
                    startActivity(intent);

                }
            }
        });


    }




    @Override
    public void onBackPressed() {
        System.exit(0);
        this.finish();
        super.onBackPressed();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        FragmentManager fragmentManager = getFragmentManager();
        if (id == R.id.nav_first_layout) {
            fragmentManager.beginTransaction()
                   .replace(R.id.content_frame
                   , new FirstFragment()).commit();

            // Handle the camera action

        } else if (id == R.id.nav_second_layout) {
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame
                            , new FirstFragment()).commit();
            showData("Incomplete Sentence");

        }  else if (id == R.id.nav_setting) {

        } else if (id == R.id.logout) {
            startActivity(new Intent(MainActivity.this,Login.class));
        } else if (id == R.id.nav_forth_layout){
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame
                            , new FirstFragment()).commit();

            showData("Reading Comprehension");

        }else if (id == R.id.nav_fifth_layout){
            fragmentManager.beginTransaction()
                    .replace(R.id.content_frame
                            , new FirstFragment()).commit();

            showData("Vocabulary Test");
        }else if (id == R.id.nav_answer) {
            startActivity(new Intent(MainActivity.this, ListAnswer.class));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    public void showData(String value){
        part.add(value);
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout
                .simple_list_item_1,part);
        mListView.setAdapter(arrayAdapter);
    }

    private void createScore() {


        SharedPreferences loadScore = getSharedPreferences(SAVE,0);
        c_score = loadScore.getInt("SaveScore",0);


        i = c_score;
        Firebase childRef  = mRef.child("score" + String.valueOf(i));
        timeStamp = new SimpleDateFormat("yyyy/MM/dd_HH:mm:ss").format(Calendar.getInstance().getTime());
        childRef.setValue(String.valueOf(sum) +  "    " + timeStamp);

        i++;

        SharedPreferences number_score = getSharedPreferences(SAVE,MODE_PRIVATE);
        SharedPreferences.Editor editor = number_score.edit();
        editor.putInt("SaveScore", i);
        editor.apply();



    }
}
