package apiwat.kitchanukit.kku.ac.th.navigation;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;

/**
 * Created by pc1 on 2/5/2560.
 */

public class Result extends Activity {

    BarChart barChart;
    TextView score;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        score = (TextView) findViewById(R.id.score);
        Bundle bundle = getIntent().getExtras();
        float score_incom = bundle.getInt("scoreIncom");
        float score_reading = bundle.getInt("scoreReading");
        float score_vocab = bundle.getInt("scoreVocab");

        float sum = score_incom + score_reading +score_vocab;
        score.setText("Your score is " + sum);
        barChart = (BarChart) findViewById(R.id.bargraph);

        ArrayList<BarEntry> barEntries = new ArrayList<>();
        barEntries.add(new BarEntry(score_incom*100/50,0));
        barEntries.add(new BarEntry(score_reading*100/20, 1));
        barEntries.add(new BarEntry(score_vocab*100/30, 2));

        BarDataSet barDataSet = new BarDataSet(barEntries, "Point");

        ArrayList<String> theDates = new ArrayList<>();
        theDates.add("incomplete Sentences");
        theDates.add("Reading");
        theDates.add("Vocab");

        BarData theData = new BarData(theDates, barDataSet);
        barChart.setData(theData);

        barChart.setTouchEnabled(true);
        barChart.setDragEnabled(true);
        barChart.setScaleEnabled(true);

    }
}
