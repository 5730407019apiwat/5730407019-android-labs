package apiwat.kitchanukit.kku.ac.th.navigation;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class StartScreen extends Activity {

    private final int delayTime = 1500;//1.text_test1 second
    private boolean endDelay = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_screen);

        //show splash screen
        Thread showsps = new Thread(){
            private long beginTime;
            public void run() {
                beginTime = System.currentTimeMillis();
                while(!endDelay){
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    endDelay = System.currentTimeMillis()-beginTime >= delayTime;
                }
                //if end time go to Main page
                Intent intent = new Intent(StartScreen.this,Login.class);
                StartScreen.this.startActivity(intent);
            };
        };
        showsps.start();
    }
}