package apiwat.kitchanukit.kku.ac.th.myapplication3;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

public class ResultActivity extends Activity {


    EditText edR;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        edR = (EditText) findViewById(R.id.edR);
        textView = (TextView) findViewById(R.id.textView);

        Intent i = getIntent();
        textView.setHint(i.getStringExtra("text_main"));

    }

    @Override
    public void finish() {

        Intent intent = new Intent();

        intent.putExtra("return", edR.getText().toString());
        setResult(RESULT_OK, intent);
        super.finish();
    }
}
