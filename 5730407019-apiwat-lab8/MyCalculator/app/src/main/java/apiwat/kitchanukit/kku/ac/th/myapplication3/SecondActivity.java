package apiwat.kitchanukit.kku.ac.th.myapplication3;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class SecondActivity extends Activity {

    TextView result2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_secondactivity);

        result2 = (TextView) findViewById(R.id.result2);
        Intent I = getIntent();
        result2.setText("Result = " + I.getExtras().getString("result"));

    }
}
