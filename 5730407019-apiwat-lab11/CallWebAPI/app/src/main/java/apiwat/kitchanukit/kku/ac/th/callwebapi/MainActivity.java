package apiwat.kitchanukit.kku.ac.th.callwebapi;

import android.app.Activity;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Iterator;

public class MainActivity extends Activity {

    TextView urlText, responseView;
    String urlVal  ;
    Button queryButton;
    URL urlAddr;
    HttpURLConnection urlConnection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        urlText = (TextView) findViewById(R.id.urlText);
        responseView = (TextView) findViewById(R.id.responseView);
        queryButton = (Button) findViewById(R.id.queryButton);

        queryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                urlVal = urlText.getText().toString();
                new RetrieveFeedTask().execute();

            }
        });

    }

    public class RetrieveFeedTask extends AsyncTask<Void, Void, String> {

        private Exception exception;

        protected void onPreExecute() {
            //progressBar.setVisibility(View.VISIBLE);
            responseView.setText("");
        }

        protected String doInBackground(Void... urls) {

            try {

                if(urlVal.contains("http")) {
                    //Log.i("INFO", "url = " + urlVal);
                     urlAddr = new URL(urlVal);
                }else {
                     urlAddr = new URL("http://maps.googleapis.com/maps/api/geocode/json?address="
                            + urlVal.replace(" ", ""));
                }
                try {
                     urlConnection =
                            (HttpURLConnection) urlAddr.openConnection();
                    BufferedReader bufferedReader =
                            new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    StringBuilder stringBuilder = new StringBuilder();
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        stringBuilder.append(line).append("\n");
                    }
                    bufferedReader.close();
                    return stringBuilder.toString();
                } finally {
                    urlConnection.disconnect();
                }
            } catch (Exception e) {
                Log.e("ERROR", e.getMessage(), e);
                return null;
            }
        }

        protected void onPostExecute(String response) {

            if (response == null) {
                response = "THERE WAS AN ERROR";
            }
            Log.i("INFO", "reponse = " + response);
            responseView.setText(response);
                try {
                    JSONObject object = (JSONObject) new
                            JSONTokener(response).nextValue();

                    if(urlVal.contains("http")) {
                        String ip = object.getString("ip");
                        responseView.setText(ip);
                    }else {
                        JSONArray results = (JSONArray) object.get("results");
                        JSONObject geometry = results.getJSONObject(0).getJSONObject("geometry").getJSONObject("location");
                        String la = geometry.getString("lat");
                        String lng = geometry.getString("lng");

                    responseView.setText("Latitude of " + urlVal + " = " + la +
                    "\nLongitude of " + urlVal + " = " + lng);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
        }
    }
}



