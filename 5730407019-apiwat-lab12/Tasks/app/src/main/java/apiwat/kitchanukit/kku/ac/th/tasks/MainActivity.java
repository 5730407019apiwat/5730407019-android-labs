package apiwat.kitchanukit.kku.ac.th.tasks;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import java.util.List;

public class MainActivity extends ActionBarActivity {
    private TextView mOutput;
    private DBHelper dh;
    private StringBuilder sb;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mOutput = (TextView) findViewById(R.id.tasks);
        dh = new DBHelper(this);
        dh.deleteAll();

        dh.insert(1, "May 4 17:00-20:00 Quiz 2");
        dh.insert(2, "May 10 13:00-16:00 Final exam");
        dh.insert(3, "May 12 13:00-16.00 Final project presentation");
        displayAllRows();

        mOutput.append("\nAdding new tasks\n");
        dh.insert(4, "May 11 13:30-15:00 Talk about Property Tech");
        displayById(4);
        dh.insert(5, "May 19 Uploading Apps to Google Play");
        displayById(5);

        mOutput.append("Updating the existing task\n");
        dh.update(3, "May 12 13:00-16:00 Final project presentation at room EN 4101");
        displayById(3);

        mOutput.append("\nDisplaying all rows\n");
        displayAllRows();
    }
    protected void displayAllRows() {
        dh.selectAll();
        retrieveAllRows();
        mOutput.append(sb.toString());
    }

    protected void displayById(int id){
        dh.selectById(id);
        retrieveAllRows();
        mOutput.append(sb.toString());
    }

    protected void retrieveAllRows(){
        List<Integer> idList = dh.getIdList();
        List<String> names = dh.getNameList();
        Log.v("MainActivity", idList.toString());
        Log.v("MainActivity", names.toString());
        int i = 0;
        sb= new StringBuilder();
        for (String name : names){
            Integer id = idList.get(i++);
            sb.append(id + "." + name + "\n");
        }
    }
}
